﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Segments;

namespace Test.SomeInterestingCases
{
    class MoreOneSegmentDataGetters : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            return Tools.GetSetup("MoreOneSegment");
        }

       

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    class OneSegmentDataGetters : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            return Tools.GetSetup("OneSegment");
        }



        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    static class Tools
    {
        public static IEnumerator<object[]> GetSetup(string dirName)
        {
            var dirInfo =
                new DirectoryInfo(
                    $"SomeInterestingCases{Path.DirectorySeparatorChar}{dirName}");
            var files = dirInfo.GetFiles("*.csv");
            var fieldReader = new FieldReader();

            return files.Select(fileInfo => new object[]
            {
                fieldReader.Get(fileInfo.FullName)
            }).GetEnumerator();
        }
    }
}
