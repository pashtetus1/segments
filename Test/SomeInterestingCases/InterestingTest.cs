﻿using System.Collections.Generic;
using Segments;
using Segments.Models;
using Xunit;

namespace Test.SomeInterestingCases
{
    public class Test
    {
        [Theory]
        [ClassData(typeof(MoreOneSegmentDataGetters))]
        public void MustTrueIfMoreOneSegment(IEnumerable<LinkNode> segments)
        {
            var cheker = new SegmentsChecker();
            var result = cheker.Check(segments);
            Assert.True(result);
        }

        [Theory]
        [ClassData(typeof(OneSegmentDataGetters))]
        public void MustFalseIfOneSegment(IEnumerable<LinkNode> segments)
        {
            var cheker = new SegmentsChecker();
            var result = cheker.Check(segments);
            Assert.False(result);
        }

    }
}
