﻿using System;
using System.Collections.Generic;
using System.Linq;
using Segments.Models;

namespace Test.Tools
{
    class SegmentsFieldGenerator
    {
        private static readonly Random Random = new Random();
        public List<LinkNode> CreateData(params int [] segmentSizes)
        {
            
            var firstNodeInSegmentNumber = 0;
            var links = new List<LinkNode>();
            foreach (var size in segmentSizes)
            {
                var segment = GenerateSegment(size, firstNodeInSegmentNumber);
                links.AddRange(segment);
                firstNodeInSegmentNumber = size + firstNodeInSegmentNumber;
            }

            return Shake(links);
        }

        private List<LinkNode> Shake(IReadOnlyCollection<LinkNode> links)
        {
            foreach (var link in links)
            {
                if (Random.Next(2) != 1) continue;
                var buffer = link.Node1Id;
                link.Node1Id = link.Node2Id;
                link.Node2Id = buffer;
            }

            return links.OrderBy(item => Random.Next()).ToList();
        }

        private static IEnumerable<LinkNode> GenerateSegment(int size, int firstNodeInSegmentNumber)
        {
            var links = new List<LinkNode>();
            for (var i = 0; i < size; i++)
            {
                var leftNodeRelativeNumber = i;
                var rightNodeRelativeNumber = i + 1;
                if (rightNodeRelativeNumber == size)
                {
                    rightNodeRelativeNumber = Random.Next(size - 2);
                }
                else if (i > 2)
                {
                    if (Random.Next(2) == 1)
                    {
                        leftNodeRelativeNumber = Random.Next(i);
                    }
                }

                var leftNodeNumber = leftNodeRelativeNumber + firstNodeInSegmentNumber;
                var rightNodeNumber = rightNodeRelativeNumber + firstNodeInSegmentNumber;
                links.Add(new LinkNode {Node1Id = $"Node{leftNodeNumber}", Node2Id = $"Node{rightNodeNumber}"});
                if (Random.Next(100) < 7)
                {
                    links.Add(new LinkNode { Node1Id = $"Node{Random.Next(i) + firstNodeInSegmentNumber}", Node2Id = $"Node{Random.Next(i) + firstNodeInSegmentNumber}" });
                }
            }

            return links;
        }


    }
}
