﻿using System.Collections.Generic;
using System.IO;
using CsvHelper;
using Segments.Models;

namespace Test.Tools
{
    static class FieldWriter
    {
        public static void WriteToFile(this IEnumerable<LinkNode> field, string filePath)
        {
            using (var textWriter = new StreamWriter(filePath))
            {
                var csvWriter = new CsvWriter(textWriter);
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.WriteRecords(field);
            }
        }
    }
}
