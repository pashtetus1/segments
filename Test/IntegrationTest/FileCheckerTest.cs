﻿using Segments;
using Test.Tools;
using Xunit;

namespace Test.IntegrationTest 
{
    public class FileCheckerTest : IClassFixture<FileFixture>
    {
        private readonly string _directoryWithTestFiles;

        public FileCheckerTest(FileFixture fileFixture)
        {
            _directoryWithTestFiles = fileFixture.DirectoryWithTestFiles;
        }

        [Theory]
        [InlineData(200, 100, 20)]
        [InlineData(200, 300)]
        [InlineData(200, 300, 500, 200)]
        public void MustTrueIfMoreOneSegment(params int[] segments)
        {
            var dataGenerator = new SegmentsFieldGenerator();
            var field = dataGenerator.CreateData(segments);
            var path = $"{_directoryWithTestFiles}{string.Join('s', segments)}.csv";
            field.WriteToFile(path);
            var result = Program.CheckFile(path);
            Assert.True(result);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(200)]
        [InlineData(1000)]
        [InlineData(2000)]
        public void MustFalseIfOneSegment(int segment)
        {
            var dataGenerator = new SegmentsFieldGenerator();
            var field = dataGenerator.CreateData(segment);
            var path = $"{_directoryWithTestFiles}{segment}.csv";
            field.WriteToFile(path);
            var result = Program.CheckFile(path);
            Assert.False(result);
        }
    }
}
