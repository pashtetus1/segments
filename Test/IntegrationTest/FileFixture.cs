﻿using System.IO;

namespace Test.IntegrationTest
{
    public class FileFixture
    {
        public readonly string DirectoryWithTestFiles = $"TestFiles{Path.DirectorySeparatorChar}";
        public FileFixture()
        {
            Directory.CreateDirectory(DirectoryWithTestFiles);
            var di = new DirectoryInfo(DirectoryWithTestFiles);
            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }
        }
    }
}
