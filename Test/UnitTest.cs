using Segments;
using Test.Tools;
using Xunit;

namespace Test
{
    public class UnitTest
    {
        [Theory]
        [InlineData(200,100,20)]
        [InlineData(200, 300)]
        [InlineData(200, 300, 500 ,200)]
        [InlineData(3000, 4000, 3000)]
        public void MustTrueIfMoreOneSegment(params int[] segments)
        {
            var dataGenerator = new SegmentsFieldGenerator();
            var field = dataGenerator.CreateData(segments);
            var cheker = new SegmentsChecker();

            var result = cheker.Check(field);
            if (!result)
            {
                field.WriteToFile($"failedTestManySegment{segments}.csv");
            }
            Assert.True(result);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(200)]
        [InlineData(1000)]
        [InlineData(13500)]
        public void MustFalseIfOneSegment(int segment)
        {
            var dataGenerator = new SegmentsFieldGenerator();
            var field = dataGenerator.CreateData(segment);
            var cheker = new SegmentsChecker();
            
            var result = cheker.Check(field);
            if (result)
            {
                field.WriteToFile($"failedTestOneSegment{segment}.csv");
            }
            Assert.False(result);
        }

        
    }
}
