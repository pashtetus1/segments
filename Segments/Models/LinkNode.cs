﻿namespace Segments.Models
{
    public class LinkNode
    {
        public string Node1Id { get; set; }
        public string Node2Id { get; set; }
    }
}
