﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Test")]
namespace Segments
{
   
    class Program
    {
        static void Main(string[] args)
        {
            string path;
            if (!args.Any())
            {
                Console.WriteLine("Print path to csv file");
                path = Console.ReadLine();
            }
            else
            {
                path = args.First();
            }

            bool moreOneSegment;
            try
            {
                moreOneSegment = CheckFile(path);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadLine();
                return;
            }
            
            Console.WriteLine($"More then one segment: {moreOneSegment}");
            Console.ReadLine();
        }

        internal static bool CheckFile(string path)
        {
            var reader = new FieldReader();
            var field = reader.Get(path);

            var checker = new SegmentsChecker();
            var moreOneSegment = checker.Check(field);

            return moreOneSegment;
        }
    }
}
