﻿using System;
using System.Collections.Generic;
using System.Linq;
using Segments.Models;
using Segments.Сhecker;

namespace Segments
{
    class SegmentsChecker
    {
        private List<LightLinkNode> _proccesingList = new List<LightLinkNode>();

        public bool Check(IEnumerable<LinkNode> links)
        {
            var lighter = new Lighter();
            _proccesingList = lighter.DoLighter(links);
            var currentLink = _proccesingList[0];
            _proccesingList.Remove(currentLink);

            PassAllBindLink(currentLink);

            return _proccesingList.Any();
        }

        private void PassAllBindLink(LightLinkNode startLink)
        {
            var currentLink = startLink;
            var linksToCheck = new List<LightLinkNode>();
            while (true)
            {

                var linkedLinks = _proccesingList.FindAll( 
                                                IsBindLink(currentLink)
                                             );
                linksToCheck.AddRange(linkedLinks);
                linkedLinks.ForEach(x=> _proccesingList.Remove(x));

                if (!linksToCheck.Any())
                {
                    break;
                }

                currentLink = linksToCheck[0];
                linksToCheck.RemoveAt(0);
            }
        }
        
        private static Predicate<LightLinkNode> IsBindLink(LightLinkNode currentLink)
        {
            return link => 
                link.Node1Id == currentLink.Node1Id
                || link.Node1Id == currentLink.Node2Id
                || link.Node2Id == currentLink.Node1Id
                || link.Node2Id == currentLink.Node2Id;
        }
    }
}
