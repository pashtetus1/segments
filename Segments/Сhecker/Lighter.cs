﻿using System.Collections.Generic;
using Segments.Models;

namespace Segments.Сhecker
{
    class Lighter
    {
        private int _lastIndex = 0;
        private readonly Dictionary<string, int> _dictionaryIntRepresent = new Dictionary<string, int>();

        public List<LightLinkNode> DoLighter(IEnumerable<LinkNode> list)
        {
            var resultLightList = new List<LightLinkNode>();

            foreach (var linkNode in list)
            {

                var node1IdRepresent = GetIntRepresent(linkNode.Node1Id);
                var node2IdRepresent = GetIntRepresent(linkNode.Node2Id);

                resultLightList.Add(new LightLinkNode
                {
                    Node1Id = node1IdRepresent,
                    Node2Id = node2IdRepresent
                });
            }

            return resultLightList;
        }

        private int GetIntRepresent(string node)
        {
            var success = _dictionaryIntRepresent.TryGetValue(node, out var currentIntRepresent);
            if (success) return currentIntRepresent;
            currentIntRepresent = ++_lastIndex;
            _dictionaryIntRepresent.Add(node, currentIntRepresent);

            return currentIntRepresent;
        }
    }
}
