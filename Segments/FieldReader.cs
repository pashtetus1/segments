﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Segments.Models;

namespace Segments
{
    class FieldReader
    {

        public IEnumerable<LinkNode> Get(string path)
        {
            using (var textWriter = new StreamReader(path))
            {
                var reader = new CsvReader(textWriter);
                reader.Configuration.Delimiter = ";";
                return reader.GetRecords<LinkNode>().ToList();
            }
        }
    }
}
